from flask import Flask
from flask_restful import abort
import re

app = Flask(__name__)

def validate(s):
    if re.match("[a-zA-Z]+$",s):
        return True
    else:
        return False

@app.route('/<string:string>', methods=['POST'])
def caseswap(string):
    if validate(string):
        return {"result": string.swapcase()}, 200
    else:
        abort(422)
    
if __name__ == "__main__":
    app.run()
