from main import app

def test_caseswap_success():
    resp = app.test_client().post("/TeStIng")
    assert resp.status_code == 200
    assert {"result": "tEsTiNG"} == resp.get_json()
    
def test_caseswap_not_found():
    resp = app.test_client().post("/")
    assert resp.status_code == 404
    
def test_caseswap_method_not_allowed():
    resp = app.test_client().get("/TeStIng")
    assert resp.status_code == 405
    
##Regexp tests

def test_caseswap_invalid_numbers():
    resp = app.test_client().post("/12345")
    assert resp.status_code == 422

def test_caseswap_invalid_chars():
    resp = app.test_client().post("/.;,@")
    assert resp.status_code == 422

def test_caseswap_invalid_mixed_chars():
    resp = app.test_client().post("/da.@")
    assert resp.status_code == 422

def test_caseswap_invalid_all_chars():
    resp = app.test_client().post("/dt2$a2.^w,@")
    assert resp.status_code == 422
    
def test_caseswap_invalid_just_space():
    resp = app.test_client().post("/ ")
    assert resp.status_code == 422
    
def test_caseswap_invalid_with_space():
    resp = app.test_client().post("/aDw ")
    assert resp.status_code == 422