FROM python:slim

RUN mkdir -p /app

WORKDIR /app

COPY ./restapi .

RUN pip install gunicorn -r requirements.txt

EXPOSE 8000

CMD gunicorn --bind 0.0.0.0:8000 -w 4 "main:app"