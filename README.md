# Case API

This API has one endpoint. It's a POST endpoint. It receives a string as path parameter and returns it with swapped casing as the body of the response. The endpoint can only process characters from the english alphabet, no numbers and no special characters. 

## Imports

If you introduce any new imports don't forget to run this command to refresh the 'requirements.txt'. This is essential, otherwise the Docker container will crash on start up due to the imports not getting downloaded. 

```bash
    pipreqs --force ./restapi
```

## Local Host

If you want to locally host the application without using docker, you can use this command to start a listen server.
You can change the number after '-w' to modify the amount of worker instances running. The recommended is 2/CPU core.
The listening IP and port will be in the terminal.

```bash
    gunicorn -w 4 "restapi.main:app"
```

## Testing

You can use this command to test the application using pytest.

```bash
    ./app.sh test
```

## Build

To build the Docker image of the application, use this command. It will run the tests and will NOT build the application if any of them fails.

```bash
    ./app.sh build
```

## Run

After successfully building the docker image you can create and start the container with this command. It will stop and delete all existing containers and start up a new one from the last built image.

```bash
    ./app.sh run
```

## Stop

To stop the running docker container use this command. This will NOT remove the container.

```bash
    ./app.sh stop
```

## Start

To start up an already existing but stopped container use this command. If no stopped container is found it will return with an error. 

```bash
    ./app.sh start
```

## Remove

To delete all containers connected to this project use this command. This will stop all running containers and delete them aswell.

```bash
    ./app.sh remove
```

## Remove image

For clean up purposes you can remove the built image from the machine using this command. This will stop and remove all containers depending on the image.

```bash
    ./app.sh rmi
```

