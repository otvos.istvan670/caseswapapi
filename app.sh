#!/bin/bash

containers () {
    docker ps -a | grep caseapi | awk '{print $1}'
}

case $1 in
    "test")
        pytest restapi/
        ;;

    "build")
        pytest ./restapi/
        if [ $? -eq 0 ]; then
            docker build ./ -t caseapi
        fi
        ;;
    
    "run")
        if [ $(containers | wc -l) -eq 0 ]; then
            docker run -d -p 8000:8000 caseapi
        else
            docker stop $(containers)
            docker rm $(containers)
            docker run -d -p 8000:8000 caseapi
        fi
        ;;
    
    "stop")
        docker stop $(containers)
        ;;
    
    "start")
        docker start $(containers)
        ;;

    "remove")
        docker stop $(containers)
        docker rm $(containers)
        ;;
    
    "rmi")
        docker stop $(containers)
        docker rm $(containers)
        docker rmi caseapi
        ;;
esac